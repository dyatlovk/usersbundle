UserBundle
=============

User managment system in Symfony 4+

Features:
- Registration
- Auth via email
- Remember me
- Password resetting
- Console commands (activate, create, promote etc)
- Standalone unit tested (no requiring symfony framework)

[![pipeline status](http://gitlab.vdev/sf_bundles/users/badges/master/pipeline.svg)](http://gitlab.vdev/sf_bundles/users/commits/master)
[![coverage report](http://gitlab.vdev/sf_bundles/users/badges/master/coverage.svg)](http://gitlab.vdev/sf_bundles/users/commits/master)


Quick start
-------------

Require the bundle with composer:
```
composer requre dyatlovk/usersbundle
```

Enable the bundle:
```php
<?php
// config/bundles.php

return [
        // ...
        new Dyatlov\UsersBundle\DyatlovUserBundle(),
        // ...
    ];
```

Create User entity class:
```php
<?php
// App/Entity/User.php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Dyatlov\UsersBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class User
 *
 * @UniqueEntity(fields="email", message="Email already taken")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="`user`")
 */
class User extends BaseUser
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
}

```

Configure your application's config:
```yaml
# config/security.yml

security:
    encoders:
        Dyatlov\UsersBundle\Model\UserInterface:
            algorithm: bcrypt

    providers:
        in_memory: { memory: ~ }
        user_provider:
            entity:
                class: App\Entity\User
                property: email
    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false
        main:
            anonymous: true
            provider: user_provider
            user_checker: dyatlov_users.checker
            logout:
                path: dyatlov.users.logout
                target: app.index
            form_login:
                login_path: dyatlov.users.login
                check_path: dyatlov.users.login
                username_parameter: login_form[email]
                password_parameter: login_form[password]
            remember_me:
                secret: '%kernel.secret%'
                lifetime: 604800 # 1 week in seconds
                path: /
                secure: true
                always_remember_me: false
                remember_me_parameter: login_form[rememberme]

    access_control:
         - { path: ^/login, roles: IS_AUTHENTICATED_ANONYMOUSLY }

```

Configure the bundle:
```yaml
# config/framework.yml

dyatlov_users:
    user_class: 'App\Entity\User'
```

Importing routes:
```yaml
#config/routes/users.yml

users:
    resource: '@DyatlovUsersBundle/Resources/config/routing/'
    type: directory
```

Update your database schema:
```bash
bin/console doctrine:schema:update --force
```