<?php

declare(strict_types=1);

use Doctrine\Bundle\DoctrineBundle\Command\Proxy\UpdateSchemaDoctrineCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\HttpFoundation\Response;

$loader = require __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/Fixtures/App/AppKernel.php';

$kernel = new AppKernel('test', false);

$application = new Application($kernel);

// let Doctrine create the database schema (i.e. the tables)
$command = new UpdateSchemaDoctrineCommand();
$application->add($command);
$input = new ArrayInput(
    ['command' => 'doctrine:schema:update', '--force' => true]
);

try {
    $command->run($input, new ConsoleOutput());
} catch (Exception $e) {
    return new Response($e->getMessage());
}
$connection = $application->getKernel()->getContainer()->get('doctrine')->getConnection();
if ($connection->isConnected()) {
    $connection->close();
}
