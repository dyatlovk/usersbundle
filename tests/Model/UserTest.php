<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Tests\Model;

use Dyatlov\UsersBundle\Model\User;
use Dyatlov\UsersBundle\Tests\TestUser;
use PHPUnit\Framework\TestCase;

/**
 * Class UserTest.
 */
class UserTest extends TestCase
{
    public function testUsername()
    {
        $user = $this->getUser();
        $this->assertNull($user->getUsername());

        $user->setUsername('vasyan');
        $this->assertSame('vasyan', $user->getUsername());
    }

    public function testEmail()
    {
        $user = $this->getUser();
        $this->assertNull($user->getEmail());

        $user->setEmail('vasyan@mail.org');
        $this->assertSame('vasyan@mail.org', $user->getEmail());
    }

    public function testGetPassword()
    {
        $user = $this->getUser();
        $user->setPassword('123');

        $this->assertSame('123', $user->getPassword());
    }

    public function testConfirmationToken()
    {
        $user = $this->getUser();
        $user->setConfirmationToken('123');

        $this->assertSame('123', $user->getConfirmationToken());
    }

    public function testIsPasswordRequestNonExpired()
    {
        $user = $this->getUser();
        $passwordRequestedAt = new \DateTime('-10 seconds');

        $user->setPasswordRequestedAt($passwordRequestedAt);

        $this->assertSame($passwordRequestedAt, $user->getPasswordRequestedAt());
        $this->assertTrue($user->isPasswordRequestNonExpired(15));
        $this->assertFalse($user->isPasswordRequestNonExpired(5));
    }

    public function testSerialize()
    {
        $user = new TestUser();
        $user->setId('25');
        $user->setUsername('user');
        $user->setPassword('123');
        $user->setEmail('user@example.com');
        $ser = $user->serialize();

        $this->assertTrue(is_string($ser));
    }

    public function testUnserialized()
    {
        $user = new TestUser();
        $ser = $this->serialized();
        $user->unserialize($ser);

        $this->assertSame('test', $user->getUsername());
        $this->assertSame('test@example.com', $user->getEmail());
        $this->assertSame('456', $user->getPassword());
    }

    /**
     * @return User
     */
    protected function getUser()
    {
        return $this->getMockForAbstractClass('Dyatlov\UsersBundle\Model\User');
    }

    protected function serialized()
    {
        return 'a:4:{i:0;s:2:"25";i:1;s:4:"test";i:2;s:3:"456";i:3;s:16:"test@example.com";}';
    }
}
