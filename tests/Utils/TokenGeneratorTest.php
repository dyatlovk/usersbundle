<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Tests;

use Dyatlov\UsersBundle\Utils\TokenGenerator;
use PHPUnit\Framework\TestCase;

/**
 * Class TokenGeneratorTest.
 */
class TokenGeneratorTest extends TestCase
{
    public function testTokenGenerator()
    {
        $token = new TokenGenerator();
        try {
            $token = $token->generateToken();
        } catch (\Exception $e) {
        }

        $this->assertTrue(is_string($token));
    }
}
