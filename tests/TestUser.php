<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Tests;

use Dyatlov\UsersBundle\Model\User;

/**
 * Class TestUser.
 */
class TestUser extends User
{
    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
