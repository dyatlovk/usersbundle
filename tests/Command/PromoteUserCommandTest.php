<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Tests\Command;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Dyatlov\UsersBundle\Model\UserInterface;
use Dyatlov\UsersBundle\Tests\Fixtures\App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class PromoteUserCommandTest.
 */
class PromoteUserCommandTest extends KernelTestCase
{
    /** @var EntityManager */
    private $em;

    private $kernel;

    public function __construct()
    {
        parent::__construct();
        $this->kernel = self::createKernel(['environment' => 'test']);
    }

    /**
     * @return string
     */
    protected static function getKernelClass(): string
    {
        require_once __DIR__ . '/../Fixtures/App/AppKernel.php';

        return 'AppKernel';
    }

    public function setUp()
    {
        $this->kernel->boot();
        $this->em = $this->kernel->getContainer()->get('doctrine')->getManager();
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testNormalExecute()
    {
        $application = new Application($this->kernel);

        /**
         * Add user if no exists.
         */
        $user = $this->em
            ->getRepository(User::class)
            ->findOneBy(['email' => $this->userData()['email']]);

        if (!$user) {
            $user = new User();
            $user->setEmail($this->userData()['email']);
            $user->setPassword($this->userData()['password']);
            $user->setUsername($this->userData()['username']);
            $user->setIsActive(true);
            $user->setRoles(['ROLE_USER']);
            $this->em->persist($user);
            $this->em->flush();
        }

        if ($user) {
            $user->setRoles(['ROLE_USER']);
            $user->setIsActive(true);
            $this->em->flush();
        }

        $command = $application->find('dyatlov_users:promote');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'email' => 'test@yandex.ru',
            [
                'decorated' => false,
                'interactive' => false,
            ],
        ]);
        $output = $commandTester->getDisplay();
        $this->assertContains('User: test@yandex.ru promoted to admin', $output);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testUserNoExists()
    {
        $application = new Application($this->kernel);

        /**
         * Remove user if exists.
         */
        $user = $this
            ->em
            ->getRepository(User::class)
            ->findOneBy(['email' => $this->userData()['email']]);

        if ($user && $user instanceof UserInterface) {
            $this->em->remove($user);
            $this->em->flush();
        }

        $command = $application->find('dyatlov_users:promote');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'email' => 'test@yandex.ru',
            [
                'decorated' => false,
                'interactive' => false,
            ],
        ]);
        $output = $commandTester->getDisplay();
        $this->assertContains('User: test@yandex.ru not found', $output);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function testUserIsPromoted()
    {
        $application = new Application($this->kernel);

        /**
         * Add user if no exists.
         */
        $user = $this->em
            ->getRepository(User::class)
            ->findOneBy(['email' => $this->userData()['email']]);

        if (!$user) {
            $user = new User();
            $user->setEmail($this->userData()['email']);
            $user->setPassword($this->userData()['password']);
            $user->setUsername($this->userData()['username']);
            $user->setIsActive(true);
            $user->setRoles(['ROLE_ADMIN']);
            $this->em->persist($user);
            $this->em->flush();
        }

        if ($user) {
            $user->setRoles(['ROLE_ADMIN']);
            $user->setIsActive(false);
            $this->em->flush();
        }

        $command = $application->find('dyatlov_users:promote');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'email' => 'test@yandex.ru',
            [
                'decorated' => false,
                'interactive' => false,
            ],
        ]);
        $output = $commandTester->getDisplay();
        $this->assertContains('User: test@yandex.ru already promoted', $output);
    }

    /**
     * @return array
     */
    private function userData(): array
    {
        return [
            'username' => 'test',
            'email' => 'test@yandex.ru',
            'password' => '123',
        ];
    }
}
