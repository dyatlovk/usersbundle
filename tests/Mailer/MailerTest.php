<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Tests\Mailer;

use Dyatlov\UsersBundle\Mailer\Mailer;
use Dyatlov\UsersBundle\Mailer\MailerInterface;
use Dyatlov\UsersBundle\Tests\Fixtures\App\Entity\User;
use PHPUnit\Framework\TestCase;
use Throwable;

class MailerTest extends TestCase
{
    protected function setUp(): void
    {
        $this->markTestIncomplete();
    }

    /**
     * @dataProvider goodEmailProvider
     *
     * @param $emailAddress
     *
     * @throws Throwable
     */
    public function testSendResetEmailMessage($emailAddress)
    {
        $mailer = $this->getMailer();
        $mailer->sendResetEmailMessage($this->getUser($emailAddress));
    }

    /**
     * @return Mailer
     */
    private function getMailer()
    {
        $symfonyMailer = $this->createMock(MailerInterface::class);

        return $symfonyMailer;
    }

    /**
     * @param $emailAddress
     *
     * @return User
     */
    private function getUser($emailAddress)
    {
        $user = new User();
        $user->setEmail($emailAddress);

        return $user;
    }

    public function goodEmailProvider()
    {
        return [
            ['foo@example.com'],
            ['foo@example.co.uk'],
        ];
    }
}
