<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Tests\Security;

use Dyatlov\UsersBundle\Model\UserInterface;
use Dyatlov\UsersBundle\Model\UserManagerInterface;
use Dyatlov\UsersBundle\Security\UserProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\User\ChainUserProvider;

class UserProviderTest extends TestCase
{
    /**
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var UserProvider
     */
    private $userProvider;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userManager = $this
            ->getMockBuilder(UserManagerInterface::class)
            ->getMock();
        $this->userProvider = new UserProvider($this->userManager);
    }

    public function testSupportsClass()
    {
        $provider1 = $this->getMockProvider();
        $provider1
            ->expects($this->once())
            ->method('supportsClass')
            ->with($this->equalTo('foo'))
            ->will($this->returnValue(false))
        ;

        $provider2 = $this->getMockProvider();
        $provider2
            ->expects($this->once())
            ->method('supportsClass')
            ->with($this->equalTo('foo'))
            ->will($this->returnValue(true))
        ;

        $provider = new ChainUserProvider([$provider1, $provider2]);
        $this->assertTrue($provider->supportsClass('foo'));
    }

    public function testLoadUserByEmail()
    {
        $user = $this->getMockBuilder(UserManagerInterface::class)->getMock();

        $this->userManager
            ->expects($this->once())
            ->method('findUserByEmail')
            ->with('admin@example.com')
            ->will($this->returnValue($user));

        $this->assertSame($user, $this->userProvider->loadUserByEmail('admin@example.com'));
    }

    public function testLoadUserByEmailNotFound()
    {
        $this->userManager
            ->expects($this->once())
            ->method('findUserByEmail')
            ->with('foo@example.com')
            ->will($this->returnValue(null));

        $this->userProvider->loadUserByEmail('foo@example.com');
    }

    public function testLoadUserByUsername()
    {
        $user = $this->getMockBuilder(UserManagerInterface::class)->getMock();
        $this->userManager
            ->expects($this->once())
            ->method('findUserByUsername')
            ->with('admin')
            ->will($this->returnValue($user));

        $this->assertSame($user, $this->userProvider->loadUserByUsername('admin'));
    }

    public function testLoadUserByUsernameNotFound()
    {
        $this->userManager
            ->expects($this->once())
            ->method('findUserByUsername')
            ->with('foo')
            ->will($this->returnValue(null));

        $this->userProvider->loadUserByUsername('foo');
    }

    public function testRefreshUserBy()
    {
        /**
         * @var UserInterface
         */
        $user = $this
            ->getMockBuilder('Dyatlov\UsersBundle\Model\User')
            ->setMethods(['getId'])
            ->getMock();

        $user->expects($this->once())
            ->method('getId')
            ->will($this->returnValue('123'));

        $refreshedUser = $this->getMockBuilder('Dyatlov\UsersBundle\Model\UserInterface')->getMock();

        $this->userManager->expects($this->once())
            ->method('findUserBy')
            ->with(['id' => '123'])
            ->will($this->returnValue($refreshedUser));

        $this->userManager->expects($this->atLeastOnce())
            ->method('getClass')
            ->will($this->returnValue(get_class($user)));

        $this->assertSame($refreshedUser, $this->userProvider->refreshUser($user));
    }

    public function testRefreshDeleted()
    {
        $user = $this->getMockForAbstractClass('Dyatlov\UsersBundle\Model\User');
        $this->userManager->expects($this->once())
            ->method('findUserBy')
            ->will($this->returnValue(null));

        $this->userManager->expects($this->atLeastOnce())
            ->method('getClass')
            ->will($this->returnValue(get_class($user)));

        $this->userProvider->refreshUser($user);
    }

    public function testRefreshInvalidUser()
    {
        $user = $this->getMockBuilder('Symfony\Component\Security\Core\User\UserInterface')->getMock();
        $this->userManager->expects($this->any())
            ->method('getClass')
            ->will($this->returnValue(get_class($user)));

        $this->userProvider->refreshUser($user);
    }

    public function testRefreshInvalidUserClass()
    {
        $user = $this->getMockBuilder('Dyatlov\UsersBundle\Model\User')->getMock();
        $providedUser = $this->getMockBuilder('Dyatlov\UsersBundle\Tests\TestUser')->getMock();

        $this->userManager->expects($this->atLeastOnce())
            ->method('getClass')
            ->will($this->returnValue(get_class($user)));

        $this->userProvider->refreshUser($providedUser);
    }

    protected function getMockProvider()
    {
        return $this
            ->getMockBuilder('Dyatlov\UsersBundle\Security\UserProvider')
            ->setConstructorArgs([$this->userManager])
            ->getMock();
    }
}
