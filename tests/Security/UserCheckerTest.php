<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Tests\Utils;

use Dyatlov\UsersBundle\Model\UserInterface;
use Dyatlov\UsersBundle\Security\UserChecker;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class UserCheckerTest.
 */
class UserCheckerTest extends TestCase
{
    /**
     * @expectedExceptionMessage Account has expired or deleted
     */
    public function testCheckPreAuth()
    {
        $userMock = $this->getUser(false);
        $checker = new UserChecker();
        $checker->checkPreAuth($userMock);
    }

    public function testCheckPreAuthAccountDisabled()
    {
        $userMock = $this->getUser(true);
        $checker = new UserChecker();
        $u = $checker->checkPreAuth($userMock);

        $this->assertFalse($u);
    }

    public function testCheckPostAuth()
    {
        $userMock = $this->getUser(true);
        $checker = new UserChecker();
        $u = $checker->checkPostAuth($userMock);

        $this->assertInstanceOf(UserInterface::class, $u);
    }

    /**
     * @param $isEnabled
     *
     * @return MockObject
     */
    private function getUser($isEnabled)
    {
        $userMock = $this->getMockBuilder('Dyatlov\UsersBundle\Model\User')->getMock();
        $userMock
            ->method('getIsActive')
            ->willReturn($isEnabled);

        return $userMock;
    }
}
