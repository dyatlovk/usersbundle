<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Tests\DependencyInjection;

use Dyatlov\UsersBundle\DependencyInjection\DyatlovUsersExtension;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Yaml\Parser;

/**
 * Class DyatlovUsersExtensionTest.
 */
class DyatlovUsersExtensionTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testUserLoadThrowsExceptionUnlessUserModelClassSet()
    {
        $loader = new DyatlovUsersExtension();
        $config = $this->getEmptyConfig();
        unset($config['user_class']);
        unset($config['email_from']);
        $loader->load([$config], new ContainerBuilder());
    }

    /**
     * @return mixed
     */
    protected function getEmptyConfig()
    {
        $yaml = <<<EOF
user_class: Acme\MyBundle\Document\User
EOF;
        $parser = new Parser();

        return $parser->parse($yaml);
    }

    protected function getLoader()
    {
        $mock = $this->getMockBuilder('Dyatlov\UsersBundle\DependencyInjection\DyatlovUsersExtension')->getMock();
        $mock->method('load');

        return $mock;
    }

    /**
     * @return ContainerBuilder
     */
    protected function getContainer()
    {
        return new ContainerBuilder();
    }
}
