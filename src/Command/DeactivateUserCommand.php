<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Command;

use Dyatlov\UsersBundle\Utils\UserManipulator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class DeactivateUserCommand.
 */
class DeactivateUserCommand extends Command
{
    private $userManipulator;

    /**
     * DeactivateUserCommand constructor.
     * @param UserManipulator $manipulator
     */
    public function __construct(UserManipulator $manipulator)
    {
        $this->userManipulator = $manipulator;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('user:deactivate')
            ->setDefinition([
                new InputArgument('email', InputArgument::REQUIRED, 'The email'),
            ])
            ->setDescription('Deactivate a user');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('User cli manipulator');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');

        switch ($this->userManipulator->deactivate($email)) {
            case 0:
                $io->error('User: ' . $email . ' not found');
                break;
            case 1:
                $io->success('User: ' . $email . ' deactivated');
                break;
            case 2:
                $io->text('User: ' . $email . ' already inactive');
                break;
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = [];

        if (!$input->getArgument('email')) {
            $question = new Question('Please choose an email:');
            $question->setValidator(function ($username) {
                if (empty($username)) {
                    throw new \Exception('Email can not be empty');
                }

                return $username;
            });
            $questions['email'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}
