<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Doctrine;

use Doctrine\Common\EventSubscriber;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\ORM\Tools\Event\GenerateSchemaEventArgs;

/**
 * Class MigrationEventSubscriber.
 */
class MigrationEventSubscriber implements EventSubscriber
{
    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents()
    {
        return [
           'postGenerateSchema',
        ];
    }

    /**
     * @param GenerateSchemaEventArgs $Args
     */
    public function postGenerateSchema(GenerateSchemaEventArgs $Args)
    {
        $Schema = $Args->getSchema();

        if (!$Schema->hasNamespace('public')) {
            try {
                $Schema->createNamespace('public');
            } catch (SchemaException $e) {
            }
        }
    }
}
