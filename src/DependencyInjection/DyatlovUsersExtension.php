<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class DyatlovUsersExtension.
 */
class DyatlovUsersExtension extends Extension
{
    /**
     * Loads a specific configuration.
     *
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('doctrine.yml');

        $loader->load('services.yml');
        $loader->load('security.yml');
        $loader->load('utils.yml');
        $loader->load('mailer.yml');

        $loader->load('reset.yml');
        $loader->load('register.yml');
        $loader->load('command.yml');

        $container->setParameter('dyatlov_users.user_class', $config['user_class']);
        $container->setParameter('dyatlov_users.email_from', $config['email_from']);
    }
}
