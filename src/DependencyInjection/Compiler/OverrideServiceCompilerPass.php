<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class OverrideServiceCompilerPass.
 */
class OverrideServiceCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $defNewService = $container->getDefinition('security.authentication.rememberme.services.simplehash');
        $defNewService->setClass('Dyatlov\UsersBundle\Security\Http\TokenBasedRememberMeServices');
    }
}
