<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Utils;

use Exception;

/**
 * Class TokenGenerator.
 */
class TokenGenerator implements TokenGeneratorInterface
{
    /**
     * @throws Exception
     */
    public function generateToken(): string
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}
