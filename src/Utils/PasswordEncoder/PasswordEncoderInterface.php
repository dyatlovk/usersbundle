<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Utils\PasswordEncoder;

use Dyatlov\UsersBundle\Model\UserInterface;

/**
 * Interface PasswordEncoderInterface.
 */
interface PasswordEncoderInterface
{
    public function hashPassword(UserInterface $user);
}
