<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Utils;

/**
 * Interface TokenGeneratorInterface.
 */
interface TokenGeneratorInterface
{
    public function generateToken();
}
