<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Utils;

use Doctrine\Persistence\ManagerRegistry;
use Dyatlov\UsersBundle\Model\User;
use Dyatlov\UsersBundle\Model\UserInterface;
use Dyatlov\UsersBundle\Model\UserManagerInterface;

/**
 * Class UserManipulator.
 */
class UserManipulator
{
    private $userManager;
    private $managerRegistry;

    /**
     * UserManipulator constructor.
     * @param ManagerRegistry $managerRegistry
     * @param UserManagerInterface $userManager
     */
    public function __construct(
        ManagerRegistry $managerRegistry,
        UserManagerInterface $userManager
    ) {
        $this->managerRegistry = $managerRegistry;
        $this->userManager = $userManager;
    }

    /**
     * Create new user.
     *
     * @param string $username
     * @param string $password
     * @param string $email
     * @param bool $active
     * @param array $role
     * @return bool|User|UserInterface
     */
    public function create(
        string $username,
        string $password,
        string $email,
        bool $active = true,
        array $role = ['ROLE_USER']
    ) {
        if ($this->exists($email)) {
            return false;
        }

        $user = $this->userManager->create();

        $user->setUsername($username);
        $user->setPlainPassword($password);
        $user->setEmail($email);
        $user->setIsActive($active);
        $user->setRoles($role);
        $this->userManager->updateUser($user);

        return $user;
    }

    /**
     * Deactivate user.
     *
     * @param string $email
     * @return int
     *             0: user not found
     *             1: user deactivated
     *             2: user already inactive
     */
    public function deactivate(string $email): int
    {
        /** @var User $user */
        $user = $this->userManager->findUserByEmail($email);

        // user not found
        if (!$user) {
            return 0;
        }

        /**
         * @var User
         */
        $user = $user[0];

        // user already inactive
        if (false === $user->getIsActive()) {
            return 2;
        }

        $user->setIsActive(false);
        $this->userManager->updateUser($user);

        return 1;
    }

    /**
     * Activate user.
     *
     * @param string $email
     * @return int
     *             0: user not found
     *             1: user activated
     *             2: user already active
     */
    public function activate(string $email): int
    {
        /** @var User $user */
        $user = $this->userManager->findUserByEmail($email);

        // user not found
        if (!$user) {
            return 0;
        }

        /** @var $user UserInterface */
        $user = $user[0];

        // user already active
        if (true === $user->getIsActive()) {
            return 2;
        }

        $user->setIsActive(true);
        $this->userManager->updateUser($user);

        return 1;
    }

    /**
     * Promote user to admin.
     * @param string $email
     * @return int
     */
    public function promote(string $email): int
    {
        /** @var User $user */
        $user = $this->userManager->findUserByEmail($email);

        // user not found
        if (!$user) {
            return 0;
        }

        $user = $user[0];

        if ($this->isAdmin($user)) {
            return 1;
        }

        $this->addRole($user, 'ROLE_ADMIN');

        return 2;
    }

    /**
     * Demote user.
     *
     * @param string $email
     * @return int
     */
    public function demote(string $email)
    {
        $user = $this->userManager->findUserByEmail($email);

        // user not found
        if (!$user) {
            return 0;
        }

        $user = $user[0];

        if (!$this->isAdmin($user)) {
            return 1;
        }

        $this->removeRole('ROLE_ADMIN', $user);

        return 2;
    }

    /**
     * Check admin role.
     *
     * @param User $user
     * @return bool
     *              false: user not found or not admin
     *              true: user is admin
     */
    public function isAdmin(User $user): bool
    {
        // user not found
        if (!$user) {
            return false;
        }

        if ($user->hasRole('ROLE_ADMIN')) {
            return true;
        }

        return false;
    }

    /**
     * Add role.
     *
     * @param User $user
     * @param $role
     * @return bool
     */
    public function addRole(User $user, $role): bool
    {
        if ($user->hasRole($role)) {
            return false;
        }

        $roles = $user->getRoles();

        $role = strtoupper($role);
        if ($role === $user::ROLE_DEFAULT) {
            return false;
        }
        if (!in_array($role, $roles, true)) {
            $roles[] = $role;
        }

        $em = $this->managerRegistry->getManagerForClass($this->userManager->getClass());
        $user->setRoles($roles);
        $em->flush();

        return true;
    }

    /**
     * Remove role.
     *
     * @param string $role
     * @param User $user
     * @return bool
     */
    public function removeRole(string $role, User $user)
    {
        if (!$user) {
            return false;
        }

        $roles = $user->getRoles();

        if (false !== $key = array_search(strtoupper($role), $roles, true)) {
            unset($roles[$key]);
            $roles = array_values($roles);
        }

        $em = $this->managerRegistry->getManagerForClass($this->userManager->getClass());
        $user->setRoles($roles);
        $em->flush();

        return true;
    }

    /**
     * Update password.
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function updatePassword(string $email, string $password): bool
    {
        /** @var User $user */
        $user = $this->userManager->findUserByEmail($email);

        // user not found
        if (!$user) {
            return false;
        }

        /**
         * @var UserInterface
         */
        $user = $user[0];
        $user->setPlainPassword($password);
        $this->userManager->updateUser($user);

        return true;
    }

    /**
     * @param string $email
     * @return bool|UserInterface|null
     */
    public function exists(string $email)
    {
        $user = $this->userManager->findUserByEmail($email);

        if (isset($user[0])) {
            return $user[0];
        }

        return false;
    }

    /**
     * Purge user.
     * @param string $email
     * @return bool
     */
    public function purge(string $email): bool
    {
        $user = $this->exists($email);
        if (false === $user) {
            return false;
        }

        $em = $this->managerRegistry->getManagerForClass($this->userManager->getClass());
        $em->remove($user);
        $em->flush();

        return true;
    }
}
