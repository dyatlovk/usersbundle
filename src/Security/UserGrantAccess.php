<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Security;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class UserGrantAccess.
 */
class UserGrantAccess
{
    private $token;

    /**
     * UserGrantAccess constructor.
     * @param TokenInterface $token
     */
    public function __construct(TokenInterface $token)
    {
        $this->token = $token;
    }

    /**
     * Denied access.
     *
     * @param $classToken
     */
    public function deny($classToken)
    {
        if (!$this->token) {
            return;
        }

        if ($this->token instanceof $classToken) {
            throw new NotFoundHttpException();
        }

        return;
    }
}
