<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Security;

use Symfony\Bridge\Doctrine\Security\User\EntityUserProvider;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Security;

/**
 * Class SecurityListener.
 */
class SecurityListener
{
    private $security;
    private $provider;

    /**
     * SecurityListener constructor.
     * @param Security $security
     * @param EntityUserProvider $entityUserProvider
     */
    public function __construct(Security $security, EntityUserProvider $entityUserProvider)
    {
        $this->security = $security;
        $this->provider[] = $entityUserProvider;
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event): void
    {
    }
}
