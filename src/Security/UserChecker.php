<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Security;

use Dyatlov\UsersBundle\Model\User;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserChecker.
 */
class UserChecker implements UserCheckerInterface
{
    /**
     * @param UserInterface $user
     * @return bool
     */
    public function checkPreAuth(UserInterface $user)
    {
        /** @var User $user */
        if (false === $user->getIsActive()) {
            throw new AccountExpiredException('Account has expired or deleted');
        }

        return false;
    }

    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function checkPostAuth(UserInterface $user)
    {
        return $user;
    }
}
