<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Security;

use Dyatlov\UsersBundle\Model\UserInterface;
use Dyatlov\UsersBundle\Model\UserManagerInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface as SecurityUserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class UserProvider.
 */
class UserProvider implements UserProviderInterface
{
    protected $userManager;

    /**
     * UserProvider constructor.
     * @param UserManagerInterface $userManager
     */
    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        $user = $this->findUserByName($username);

        if (!$user) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        return $user;
    }

    /**
     * Load the user by email.
     *
     * @param string $email
     * @return UserInterface
     *
     */
    public function loadUserByEmail(string $email)
    {
        $user = $this->findUser($email);

        if (!$user) {
            throw new UsernameNotFoundException(sprintf('User email "%s" does not exist.', $email));
        }

        return $user;
    }

    /**
     * Refreshes the user.
     *
     * @param SecurityUserInterface $user
     * @return UserInterface
     */
    public function refreshUser(SecurityUserInterface $user)
    {
        if (!$user instanceof UserInterface) {
            throw new UnsupportedUserException(sprintf('Expected an instance of DyatlovUsersBundle\Model\UserInterface, but got "%s".', get_class($user)));
        }

        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(sprintf('Expected an instance of %s, but got "%s".', $this->userManager->getClass(), get_class($user)));
        }

        if (null === $reloadedUser = $this->userManager->findUserBy(['id' => $user->getId()])) {
            throw new UsernameNotFoundException(sprintf('User with ID "%s" could not be reloaded.', $user->getId()));
        }

        return $reloadedUser;
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        $userClass = $this->userManager->getClass();

        return $userClass === $class || is_subclass_of($class, $userClass);
    }

    /**
     * @param $email
     *
     * @return UserInterface|null
     */
    protected function findUser($email)
    {
        return $this->userManager->findUserByEmail($email);
    }

    /**
     * @param $name
     *
     * @return UserInterface|null
     */
    protected function findUserByName($name)
    {
        return $this->userManager->findUserByUsername($name);
    }
}
