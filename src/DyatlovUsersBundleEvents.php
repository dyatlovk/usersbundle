<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle;

/**
 * Class DyatlovUsersBundleEvents.
 */
final class DyatlovUsersBundleEvents
{
    const REGISTER_INIT = 'dyatlov.user_register.init';

    const REGISTER_SUCCESS = 'dyatlov.user_register.success';

    const REGISTER_FAILED = 'dyatlov.user_register.failed';

    const REGISTER_COMPLETE = 'dyatlov.user_register.complete';

    const LOGIN_INIT = 'dyatlov.user_login.init';

    const LOGIN_SUCCESSFULLY = 'dyatlov.user_login.successfully';

    const LOGIN_FAILED = 'dyatlov.user_login.failed';

    const RESET_INIT = 'dyatlov.user_reset.init';

    const RESET_EMAIL_SEND = 'dyatlov.user_reset.email_send';

    const RESET_COMPLETE = 'dyatlov.user_reset.complete';

    const RESET_NON_EXPIRED = 'dyatlov.user_reset.non_expired';

    const USER_CREATED = 'dyatlov.user.created';

    const USER_ACTIVATED = 'dyatlov.user.activated';

    const USER_DEACTIVATED = 'dyatlov.user.deactivated';

    const USER_PROMOTED = 'dyatlov.user.promoted';

    const USER_DEMOTED = 'dyatlov.user.demoted';

    const USER_PASSWORD_CHANGED = 'dyatlov.user.demoted';
}
