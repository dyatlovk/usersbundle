<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Controller;

use Dyatlov\UsersBundle\Security\UserGrantAccess;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Security;

/**
 * Class UserBaseController.
 */
class UserBaseController extends AbstractController
{
    protected $token;
    protected $access;
    protected $user;

    /**
     * UserBaseController constructor.
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->token = $security->getToken();
        $this->user = $this->token->getUser();
        $this->access = new UserGrantAccess($this->token);
    }
}
