<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Controller;

use Dyatlov\UsersBundle\Form\UserType;
use Dyatlov\UsersBundle\Model\UserManager;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\RememberMeToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Security;

/**
 * Class RegisterController.
 */
class RegisterController extends UserBaseController
{
    protected $um;

    /**
     * RegisterController constructor.
     * @param Security $security
     * @param UserManager $um
     */
    public function __construct(
        Security $security,
        UserManager $um
    ) {
        parent::__construct($security);
        $this->um = $um;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function index(Request $request): Response
    {
        /* If user is logged return 404 */
        $this->access->deny(UsernamePasswordToken::class);
        $this->access->deny(RememberMeToken::class);

        $user = $this->um->create();
        $form = $this->createForm(UserType::class);
        $form->setData($user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->um->updateUser($user);

            return $this->redirectToRoute('app.index');
        }

        return $this->render('@DyatlovUsers/register/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
