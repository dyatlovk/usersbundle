<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Controller;

use DateTime;
use Doctrine\Persistence\ManagerRegistry;
use Dyatlov\UsersBundle\Form\ResetType;
use Dyatlov\UsersBundle\Form\UpdateType;
use Dyatlov\UsersBundle\Mailer\Mailer;
use Dyatlov\UsersBundle\Model\User as User;
use Dyatlov\UsersBundle\Model\UserManagerInterface;
use Dyatlov\UsersBundle\Utils\TokenGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\RememberMeToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Security;
use Throwable;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class ResetController.
 */
class ResetController extends UserBaseController
{
    /** password request retry time */
    const RETRY_TTL = 2 * 3600;

    protected $tokenGenerator;
    protected $userManager;
    protected $mailer;
    private $managerRegistry;

    /**
     * ResetController constructor.
     * @param Security $security
     * @param ManagerRegistry $managerRegistry
     * @param TokenGeneratorInterface $tokenGenerator
     * @param UserManagerInterface $userManager
     * @param Mailer $mailer
     */
    public function __construct(
        Security $security,
        ManagerRegistry $managerRegistry,
        TokenGeneratorInterface $tokenGenerator,
        UserManagerInterface $userManager,
        Mailer $mailer
    ) {
        parent::__construct($security);
        $this->managerRegistry = $managerRegistry;
        $this->tokenGenerator = $tokenGenerator;
        $this->userManager = $userManager;
        $this->mailer = $mailer;
    }

    /**
     * Request reset.
     *
     * @param Request $request
     * @return Response
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Throwable
     */
    public function index(Request $request)
    {
        /* If user is logged return 404 */
        $this->commonGrantAccess();

        $form = $this->createForm(ResetType::class);
        $form->handleRequest($request);

        $error = '';
        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->getData()['email'];
            /** @var $user User */
            $user = $this->userManager->findUserByEmail($email);

            if (!$user) {
                $error = 'User not found';
            }

            $em = $this->managerRegistry->getManagerForClass($this->userManager->getClass());
            if ($user) {
                if ($user->isPasswordRequestNonExpired(self::RETRY_TTL)) {
                    $error = 'Password request non expired';
                }
                if (!$user->isPasswordRequestNonExpired(self::RETRY_TTL)) {
                    $user->setPasswordRequestedAt(new DateTime());
                    $user->setConfirmationToken($this->tokenGenerator->generateToken());
                    $this->mailer->sendResetEmailMessage($user);
                    $em->flush();

                    return new RedirectResponse($this->generateUrl('dyatlov.users.reset_check', ['email' => $user->getEmail()]));
                }
            }
        }

        return $this->render('@DyatlovUsers/reset/index.html.twig', [
            'form' => $form->createView(),
            'error' => $error,
        ]);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function check(Request $request): Response
    {
        $email = $request->query->get('email');
        $requestToken = $request->query->get('token');

        if (!$email) {
            return new RedirectResponse($this->generateUrl('dyatlov.users.reset'));
        }

        /** @var $user User */
        $user = $this->userManager->findUserByEmail($email);

        if (is_null($user)) {
            return new RedirectResponse($this->generateUrl('dyatlov.users.reset'));
        }

        $token = $user->getConfirmationToken();

        if (null === $token) {
            return new RedirectResponse($this->generateUrl('dyatlov.users.reset'));
        }

        if ($user->isPasswordRequestNonExpired(self::RETRY_TTL) && $requestToken) {
            return new RedirectResponse($this->generateUrl('dyatlov.users.reset_update', ['token' => $token]));
        }

        return $this->render('@DyatlovUsers/reset/check.html.twig', ['email' => $email]);
    }

    /**
     * Update password.
     *
     * @param Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        /* If user is logged return 404 */
        $this->commonGrantAccess();

        $token = $request->query->get('token');
        if (null === $token) {
            return new RedirectResponse($this->generateUrl('dyatlov.users.reset'));
        }

        /** @var $user User */
        $user = $this->userManager->findUserByConfirmationToken($token);

        if (is_null($user)) {
            return new RedirectResponse($this->generateUrl('dyatlov.users.reset'));
        }

        $user = $user[0];

        $form = $this->createForm(UpdateType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->getData()['plainPassword'];
            $user->setConfirmationToken(null);
            $user->setPlainPassword($password);
            $this->userManager->updatePassword($user);
            $this->userManager->updateUser($user);

            return new RedirectResponse($this->generateUrl('app.index'));
        }

        return $this->render('@DyatlovUsers/reset/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Common grant access.
     */
    protected function commonGrantAccess()
    {
        $this->access->deny(UsernamePasswordToken::class);
        $this->access->deny(RememberMeToken::class);
    }
}
