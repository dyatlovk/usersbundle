<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Controller;

use Dyatlov\UsersBundle\Form\LoginType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\RememberMeToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class LoginController.
 */
class LoginController extends UserBaseController
{
    private $authUtils;

    /**
     * LoginController constructor.
     * @param Security $security
     * @param AuthenticationUtils $authenticationUtils
     */
    public function __construct(
        Security $security,
        AuthenticationUtils $authenticationUtils
    ) {
        parent::__construct($security);
        $this->authUtils = $authenticationUtils;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        /* If user is logged return 404 */
        $this->access->deny(UsernamePasswordToken::class);
        $this->access->deny(RememberMeToken::class);

        /** @var $session Session */
        $session = $request->getSession();

        $authErrorKey = Security::AUTHENTICATION_ERROR;

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        $form = $this->createForm(LoginType::class);
        $form->handleRequest($request);

        return $this->render('@DyatlovUsers/login.html.twig', [
            'error' => $error,
            'form' => $form->createView(),
        ]);
    }

    public function logout(): void
    {
        return;
    }
}
