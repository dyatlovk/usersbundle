<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Model;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectRepository;
use Dyatlov\UsersBundle\Model\User as User;
use Dyatlov\UsersBundle\Utils\PasswordEncoder\PasswordEncoder;
use Exception;

/**
 * Class UserManager.
 */
class UserManager implements UserManagerInterface
{
    private $passwordUpdater;
    private $class;
    private $managerRegistry;

    /**
     * UserManager constructor.
     *
     * @param PasswordEncoder $passwordUpdater
     * @param ManagerRegistry $managerRegistry
     * @param $class
     */
    public function __construct(PasswordEncoder $passwordUpdater, ManagerRegistry $managerRegistry, $class)
    {
        $this->passwordUpdater = $passwordUpdater;
        $this->managerRegistry = $managerRegistry;
        $this->class = $class;
    }

    /**
     * @return UserInterface|mixed
     */
    public function create()
    {
        $class = $this->getClass();

        $user = new $class();

        return $user;
    }

    /**
     * Find by confirmation token.
     *
     * @return User|object|null
     */
    public function findUserByConfirmationToken(string $token)
    {
        return $this->findUserBy(['confirmation_token' => $token]);
    }

    /**
     * Find by email.
     *
     * @return User|object|null
     */
    public function findUserByEmail(string $email)
    {
        return $this->findUserBy(['email' => $email]);
    }

    /**
     * @throws Exception
     */
    public function updatePassword(UserInterface $user)
    {
        $this->passwordUpdater->hashPassword($user);
    }

    /**
     * @return UserInterface|null
     */
    public function findUserByUsername(string $username)
    {
        return $this->findUserBy(['username' => $username]);
    }

    /**
     * @return string
     */
    public function getClass()
    {
        $em = $this->managerRegistry->getManagerForClass($this->class);
        if (false !== strpos($this->class, ':')) {
            $metadata = $em->getClassMetadata($this->class);
            $this->class = $metadata->getName();
        }

        return $this->class;
    }

    /**
     * @param bool $andFlush
     *
     * @throws Exception
     */
    public function updateUser(UserInterface $user, $andFlush = true)
    {
        $em = $this->managerRegistry->getManagerForClass($this->class);
        $this->updatePassword($user);
        $em->persist($user);
        if ($andFlush) {
            $em->flush();
        }
    }

    /**
     * @param array $criteria
     * @return UserInterface|object[]|null
     */
    public function findUserBy(array $criteria)
    {
        return $this->getRepository()->findOneBy($criteria);
    }

    /**
     * @return ObjectRepository
     */
    protected function getRepository()
    {
        $em = $this->managerRegistry->getManagerForClass($this->class);

        return $em->getRepository($this->getClass());
    }
}
