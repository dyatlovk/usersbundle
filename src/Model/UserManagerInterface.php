<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Model;

/**
 * Interface UserManagerInterface.
 */
interface UserManagerInterface
{
    /**
     * @return UserInterface
     */
    public function create();

    /**
     * @return string
     */
    public function getClass();

    /**
     * @param string $token
     * @return UserInterface|null
     */
    public function findUserByConfirmationToken(string $token);

    /**
     * @param string $email
     * @return UserInterface|null
     */
    public function findUserByEmail(string $email);

    /**
     * @param string $username
     * @return UserInterface|null
     */
    public function findUserByUsername(string $username);

    public function updateUser(UserInterface $user);

    public function updatePassword(UserInterface $user);

    /**
     * @param array $criteria
     * @return UserInterface|null
     */
    public function findUserBy(array $criteria);
}
