<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

/**
 * Interface DyatlovUserInterface.
 */
interface UserInterface extends BaseUserInterface, \Serializable
{
    const ROLE_DEFAULT = 'ROLE_USER';

    public function getId();

    public function setUsername(string $username);

    public function getUsername();

    public function getSalt();

    public function getPlainPassword();

    public function setPlainPassword(string $password);

    public function getPassword();

    public function setPassword(string $password);

    public function getRoles();

    public function setRoles(array $roles);

    public function eraseCredentials();

    public function serialize();

    public function unserialize($serialized);

    public function getEmail();

    public function setEmail(string $email);

    public function getIsActive();

    public function setIsActive(bool $flag);

    public function hasRole(string $role);

    public function getPasswordRequestedAt();

    public function setPasswordRequestedAt(\DateTimeInterface $date);

    public function isPasswordRequestNonExpired(int $ttl);

    public function setConfirmationToken(string $token);

    public function getConfirmationToken();
}
