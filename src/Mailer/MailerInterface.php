<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Mailer;

use Dyatlov\UsersBundle\Model\User as User;

/**
 * Interface MailerInterface.
 */
interface MailerInterface
{
    public function sendResetEmailMessage(User $user);
}
