<?php

declare(strict_types=1);

namespace Dyatlov\UsersBundle\Mailer;

use Dyatlov\UsersBundle\Model\User as User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Throwable;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Symfony\Component\Mailer\MailerInterface as EmailService;

/**
 * Class Mailer.
 */
class Mailer implements MailerInterface
{
    private $mailer;
    private $template;
    private $router;
    private $container;

    /**
     * Mailer constructor.
     * @param EmailService $mailer
     * @param Environment $tpl
     * @param UrlGeneratorInterface $router
     * @param ContainerInterface $container
     */
    public function __construct(EmailService $mailer, Environment $tpl, UrlGeneratorInterface $router, ContainerInterface $container)
    {
        $this->template = $tpl;
        $this->router = $router;
        $this->container = $container;
        $this->mailer = $mailer;
    }

    /**
     * @param User $user
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Throwable
     */
    public function sendResetEmailMessage(User $user)
    {
        $token = $user->getConfirmationToken();
        $url = $this->router->generate('dyatlov.users.reset_check', ['token' => $token, 'email' => $user->getEmail()], UrlGeneratorInterface::ABSOLUTE_URL);
        $this->sendEmailMessage(
            '@DyatlovUsers/reset/email.txt.twig',
            $this->container->getParameter('dyatlov_users.email_from'),
            $user->getEmail(), [
                'user' => $user,
                'url' => $url,
            ]);
    }

    /**
     * @param string $templateName
     * @param string $fromEmail
     * @param string $toEmail
     * @param array $context
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Throwable
     */
    protected function sendEmailMessage(string $templateName, string $fromEmail, string $toEmail, array $context)
    {
        $template = $this->template->load($templateName);
        $subject = $template->renderBlock('subject', $context);
        $body = $template->renderBlock('body', $context);

        $message = (new Email())
            ->subject($subject)
            ->from($fromEmail)
            ->to($toEmail)
            ->text($body);

        $this->mailer->send($message);
    }
}
